displaySelectStoryButtons();

function displaySelectStoryButtons() {
	for (let i = 0; i < stories.length; i++) {
		let selectStoryButton = createNewSelectStoryButton(stories[i]);
		selectStoryButton.addEventListener('click', onButtonClick, false);
	}
}

function createNewSelectStoryButton(story) {
	let button = document.createElement('button');
	button.innerHTML = "START";
	button.setAttribute("nextSceneId", story.storyId);
	document.getElementsByClassName("buttonContainer")[0].appendChild(button);
	return button;
}

function onButtonClick() {
	localStorage.setItem('storyid', this.getAttribute("nextsceneid"));
	window.location.replace("../StoryPage/story.html");
}

/*
  Slidemenu
*/
(function () {
    var $body = document.body
        , $menu_trigger = $body.getElementsByClassName("menu-trigger")[0];

    if (typeof $menu_trigger !== 'undefined') {
        $menu_trigger.addEventListener('click', function () {
            $body.className = ($body.className === 'menu-active') ? '' : 'menu-active';
        });
    }

}).call(this);

var menuBtn = document.querySelector('.menu-btn');
var nav = document.querySelector('nav');
var lineOne = document.querySelector('nav .menu-btn .line--1');
var lineTwo = document.querySelector('nav .menu-btn .line--2');
var lineThree = document.querySelector('nav .menu-btn .line--3');
var link = document.querySelector('nav .nav-links');
menuBtn.addEventListener('click', () => {
    nav.classList.toggle('nav-open');
    lineOne.classList.toggle('line-cross');
    lineTwo.classList.toggle('line-fade-out');
    lineThree.classList.toggle('line-cross');
    link.classList.toggle('fade-in');
})

