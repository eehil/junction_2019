var currentStory;
var imgElement;
var descriptionElement;

loadStaticElements()
generateOptionButtons();

function loadStaticElements() { // TODO find a better name for this function
	// currentStory = loadStoryWithId(1); 
	currentStory = loadStoryWithId(localStorage.getItem('storyid')); 
	imgElement = document.getElementById('storyImage'); 
	descriptionElement = document.getElementById("story");
}

function generateOptionButtons() {
	let firstScene = currentStory.scenes[0];
	for (let i = 0; i < firstScene.buttons.length; i++) {
		let button = createOptionButton();
	}
	updateImage(firstScene);
	updateDescription(firstScene);
	var buttons = document.getElementsByClassName("option");
	updateButtons(buttons, firstScene);
}

function onButtonClick() {
	let sceneId = this.getAttribute("nextsceneid");
	let nextScene = findSceneWithId(sceneId);

	updateImage(nextScene);
	updateDescription(nextScene);
	updateAllButtons(nextScene);
}

function findSceneWithId(sceneId) {
	for (let i = 0; i < currentStory.scenes.length; i++) {
		let scene = currentStory.scenes[i];
		if (scene.sceneId == sceneId) {
			console.log(scene);
			return scene;
		}
	}
}

/*
	Load image with proper name and display it instead of old one.
*/
function updateImage(scene) {
    imgElement.src = "../img/story_" + currentStory.storyId + "_scene_" + scene.sceneId + ".jpg";
}

/*
	Update inner html of description element
*/
function updateDescription(scene) {
    descriptionElement.innerHTML = scene.text;
}

/*
	Update buttons.
	If game ended then render just 'back to Home Page' button.
	Else display buttons matching new scene.
*/
function updateAllButtons(scene) {
	removeAllButtons();
	if (gameEnded(scene)) {
		createBackToHomePageButton();
		return;
	}
	// TODO optimize by retrieving buttons from createRequiredButtons function call instead of looking them up in HTML;
	createRequiredButtons(scene.buttons.length);
	var buttons = document.getElementsByClassName("option");
	updateButtons(buttons, scene);
}

function removeAllButtons() {
	const buttons = document.getElementsByClassName("option");
	const buttonsToRemove = buttons.length;
	for (let i = 0; i < buttonsToRemove; i++) {
		var buttonToRemove = buttons[0];
		buttonToRemove.parentNode.removeChild(buttonToRemove);
	}
}

function gameEnded(scene) { // scene has no options to choose from
	return scene.buttons.length == 0;
}

function createRequiredButtons(numberOfButtons) {
	for (let i = 0; i < numberOfButtons; i++) {
		createOptionButton();
	}
}

function createOptionButton() {
	let button = document.createElement('button');
	button.innerHTML = "           ";
	document.getElementsByTagName("body")[0].appendChild(button);
	button.className += "option";
	button.addEventListener('click', onButtonClick, false);
	return button;
}

/*
	Update buttons - inner html and attribute.
	Each button has attribute 'nextSceneId' which must be updated.
*/
function updateButtons(htmlButtons, scene) {
	for (let i = 0; i < htmlButtons.length; i++) {
		let htmlButton = htmlButtons[i];
		htmlButton.innerHTML = scene.buttons[i].text;
		htmlButton.setAttribute("nextsceneid", scene.buttons[i].nextScene);
	}
}

function createBackToHomePageButton() {
	let button = createOptionButton();
	button.innerHTML = "Back to Home Page";
	button.removeEventListener('click', onButtonClick);
	button.addEventListener('click', redirectToHomePage, false);
	document.getElementsByTagName("body")[0].appendChild(button);
}

function redirectToHomePage() {
	window.location.replace("../HomePage/home.html");
}